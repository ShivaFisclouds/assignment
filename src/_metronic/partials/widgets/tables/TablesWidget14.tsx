import React from "react";
import { Product } from "../../../../app/pages/MyProduct";

interface TableWidget14Props {
  data: Product[] | undefined;
}

const TableWidget14: React.FC<TableWidget14Props> = ({ data }) => {
  return (
    <div className="container">
      <div className="table-responsive">
        <table className="table table-hover">
          <tbody>
            {data?.map((product) => (
              <tr key={product.id}>
                <td>{product.title}</td>
                <td>
                  <div className="image-cell">
                    <img
                      src={product.image}
                      alt={product.title}
                      className="small-image img-fluid rounded"
                      style={{
                        maxWidth: "100px",
                        maxHeight: "100px",
                        transition: "transform 0.3s", 
                      }}
                      onMouseOver={(e) => (e.currentTarget.style.transform = "scale(1.1)")}
                      onMouseOut={(e) => (e.currentTarget.style.transform = "scale(1)")} 
                    />
                  </div>
                </td>
                <td>${product.price}</td>
                <td>{product.category}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default TableWidget14;
