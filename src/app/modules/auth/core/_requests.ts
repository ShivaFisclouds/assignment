import axios from 'axios'
import {AuthModel, UserModel} from './_models'

const API_URL = process.env.REACT_APP_API_URL

const REACT_APP_API_URL_ONE = 'https://jsonplaceholder.typicode.com/posts';
const REACT_APP_API_URL_ECOM = "https://fakestoreapi.com/products"


export const GET_USER_BY_ACCESSTOKEN_URL = `${API_URL}/verify_token`
export const LOGIN_URL = `${API_URL}/login`
export const REGISTER_URL = `${API_URL}/register`
export const REQUEST_PASSWORD_URL = `${API_URL}/forgot_password`

// Server should return AuthModel
export function login(email: string, password: string) {
  return axios.post<AuthModel>(LOGIN_URL, {
    email,
    password,
  })
}

// Server should return AuthModel
export function register(
  email: string,
  firstname: string,
  lastname: string,
  password: string,
  password_confirmation: string
) {
  return axios.post(REGISTER_URL, {
    email,
    first_name: firstname,
    last_name: lastname,
    password,
    password_confirmation,
  })
}

// Server should return object => { result: boolean } (Is Email in DB)
export function requestPassword(email: string) {
  return axios.post<{result: boolean}>(REQUEST_PASSWORD_URL, {
    email,
  })
}

export function getUserByToken(token: string) {
  return axios.post<UserModel>(GET_USER_BY_ACCESSTOKEN_URL, {
    api_token: token,
  })
}


// Function to fetch data from the API
export function fetchDataFromApi() {
  return axios.get(REACT_APP_API_URL_ONE)
    .then(response => {
      return response.data;
    })
    .catch(error => {
      console.error('Error fetching data:', error);
      throw error; 
    });
}


// Function to fetch data from the API of E-commerce Products
export function fetchDataFromApiEcommerce() {
  return axios.get(REACT_APP_API_URL_ECOM)
    .then(response => {
      return response.data;
    })
    .catch(error => {
      console.error('Error fetching data:', error);
      throw error; 
    });
}

// export function fetchCategoriesFromApi() {
//   // Replace with the actual API endpoint for fetching categories
//   const apiUrl = `${REACT_APP_API_URL_ECOM}/categories`;

//   return axios
//     .get(apiUrl)
//     .then((response) => {
//       return response.data.categories; // Assuming the API response has a "categories" property
//     })
//     .catch((error) => {
//       console.error("Error fetching categories:", error);
//       throw error;
//     });
// }








