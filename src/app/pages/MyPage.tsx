import React from "react";
import { useQuery } from "react-query";
import { fetchDataFromApi } from "../../app/modules/auth/core/_requests";

interface Item {
  id: number;
  title: string;
  body: string;
}

export default function MyPage() {
  const { data, error, isLoading } = useQuery<Item[], Error>("myData", fetchDataFromApi);

  if (isLoading) {
    return <p>Loading data...</p>;
  }

  if (error) {
    return <p>Error fetching data: {error.message}</p>;
  }

  return (
    <div>
      {data ? (
        <div>
          <div className="card-container">
            {data.map((item) => (
              <div className="card" key={item.id}>
                <div className="card-header">{item.title}</div>
                <div className="card-body">
                  <p className="card-text">{item.body}</p>
                </div>
              </div>
            ))}
          </div>
        </div>
      ) : null}
    </div>
  );
}
