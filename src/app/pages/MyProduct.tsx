import React, { useState } from "react";
import { useQuery } from "react-query";
import { fetchDataFromApiEcommerce } from "../../app/modules/auth/core/_requests";
import TableWidget14 from "../../_metronic/partials/widgets/tables/TablesWidget14";

export interface Product {
  id: number;
  title: string;
  price: number;
  image: string;
  category: string;
}

export default function MyProduct() {
  const { data, isLoading, isError } = useQuery<Product[]>(
    "ecommerceData",
    fetchDataFromApiEcommerce
  );
  const [searchText, setSearchText] = useState<string>("");
  const [selectedCategory, setSelectedCategory] = useState<string>("");
  const [sortOrder, setSortOrder] = useState<"asc" | "desc">("asc");

  if (isLoading) {
    return <div>Loading...</div>;
  }

  if (isError) {
    return <div>Error fetching data</div>;
  }


  const filteredAndSortedData = data
    ? data
        .filter((product) =>
          product.title.toLowerCase().includes(searchText.toLowerCase()) &&
          (selectedCategory === "" || product.category === selectedCategory)
        )
        .sort((a, b) => {
          if (sortOrder === "asc") {
            return a.price - b.price;
          } else {
            return b.price - a.price;
          }
        })
    : [];

  const categories = [
    "electronics",
    "jewelery",
    "men's clothing",
    "women's clothing",
  ];

  const handleCategoryChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
    setSelectedCategory(event.target.value);
  };

  const toggleSortOrder = () => {
    setSortOrder(sortOrder === "asc" ? "desc" : "asc");
  };

  return (
    <>
      <div className="search-container">
        <input
          type="text"
          placeholder="Search..."
          value={searchText}
          onChange={(e) => setSearchText(e.target.value)}
        />
      </div>
      <div className="filter-container">
        <select value={selectedCategory} onChange={handleCategoryChange}>
          <option value="">All Categories</option>
          {categories.map((category) => (
            <option key={category} value={category}>
              {category}
            </option>
          ))}
        </select>
        <button onClick={toggleSortOrder}>
          {sortOrder === "asc" ? "Sort by Price (Low to High)" : "Sort by Price (High to Low)"}
        </button>
      </div>
      <div className="product-list">
        {filteredAndSortedData.map((product) => (
          <TableWidget14 key={product.id} data={[product]} />
        ))}
      </div>
    </>
  );
}
